let players = [];

gameContainer.css('max-width', `${gameWindowW}px`);
gameContainer.css('max-height', `${gameWindowH}px`);

if (_DEV) document.body.className = 'dev';

$(document).ready(() => {
    player = new Player('user', true);
    // bot = new Player('enemy');
    // bot.setPosition(2400, 2000);

    // players.push(player);
    // players.push(bot);

    let ticks = 0;

    let clock = new Date().getTime();

    // GAME LOOP
    setInterval(() => {
        if (!gameStarted) return;

        let elapsed = new Date().getTime() - clock;
        clock = new Date().getTime();

        updateMap(currentMap);

        player.update(ticks, elapsed);

        players.forEach(player => {
            if (!player) return;
            player.update(ticks, elapsed);
            player.draw();
        });

        bindUserView();

        drawBullets(elapsed);

        initHUD();

        if(_DEV) {
            devTools();
        }

        ticks++;
        if (ticks > 1000) ticks = 0;
    }, 10);
});
