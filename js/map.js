let spawn1Coords = [0, 0];
let spawn2Coords = [0, 0];

const updateMap = () => {
    $(".block").remove();

    const _X = Math.floor(mapOffsetX / blockSize);
    const _Y = Math.floor(mapOffsetY / blockSize);

    const renderX = Math.round(gameWindowW / blockSize) + 1;
    const renderY = Math.round(gameWindowH / blockSize) + 1;

    for (let i = _X; i < _X + renderX; i++) {
        if (i > mapSizeX - 1) break;

        for (let j = _Y; j < _Y + renderY; j++) {
            if (j > mapSizeY - 1) break;

            const block = document.createElement('div');
            block.className = 'block';
            block.style.width = `${blockSize}px`;
            block.style.height = `${blockSize}px`;
            block.style.left = `${i * blockSize - mapOffsetX}px`;
            block.style.top = `${j * blockSize - mapOffsetY}px`;
            block.setAttribute('left', i);
            block.setAttribute('top', j);
            if (blocks[j][i] === 1) block.className = 'block black';
            block.innerHTML = '<div class="block-wrap"></div>';
            $('.border').append(block);
        }
    }
};

const drawMap = (m) => {
    mapOffsetX = 0;
    mapOffsetY = 0;
    for (let i = 0; i < mapSizeX; i++) {
        for (let j = 0; j < mapSizeY; j++) {
            if (!blocks[j]) blocks[j] = [];
            if (!blocks[j][i]) blocks[j][i] = 0;

            // blocks[j][i] = rand(0, 10) > 7 ? 1 : 0;

            blocks[j][i] = m[j][i];
            if (blocks[j][i] < 0) {
                if (blocks[j][i] === -1) {
                    spawn1Coords = [i * blockSize, j * blockSize];
                }
                if (blocks[j][i] === -2) {
                    spawn2Coords = [i * blockSize, j * blockSize];
                }
                blocks[j][i] = 0;
            }
        }
    }

    $(".block").remove();

    for (let i = 0; i < mapSizeX; i++) {
        for (let j = 0; j < mapSizeY; j++) {
            const block = document.createElement('div');
            block.className = 'block';
            block.style.left = `${i * blockSize}px`;
            block.style.top = `${j * blockSize}px`;
            if (blocks[j][i] === 1) block.className = 'block black';
            $('.border').append(block);
        }
    }

    player.onMapChanged();
};

// drawMap(map_de_dust);

const maps = {
    lol: map_lol,
    de_dust: map_de_dust,
    tvoya_mama: map_tvoya_mama,
    islam: map_islam,
    huy: map_huy,
};
