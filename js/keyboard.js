let lastKeyDown = null;

const moveEvent = (key, state) => {
    switch(key) {
        case 82:
            player.reload();
            break;
        case 87:
            player.isMoving[0] = state; // top
            break;
        case 68:
            player.isMoving[1] = state; // right
            break;
        case 83:
            player.isMoving[2] = state; // bottom
            break;
        case 65:
            player.isMoving[3] = state; // left
            break;
    }
};

const keydown = (event) => {
    if (!gameStarted) return;
    moveEvent(event.keyCode, true);
    lastKeyDown  = event.keyCode;
};

const keyup = (event) => {
    if (!gameStarted) return;
    moveEvent(event.keyCode, false);
    lastKeyDown = null;
};

$(window).on('keydown', keydown);
$(window).on('keyup', keyup);
