const _DEV = false;

const server = 'http://192.168.1.74:3003';

const gameWindowW = window.innerWidth;
const gameWindowH = window.innerHeight;

const raysCount = 100;

// const gameWindowW = 800;
// const gameWindowH = 600;

const gameContainer = $(".border");

const speed = 2;
const bulletSpeed = 18;

const blockSize = 200;

const mapSizeX = 16;
const mapSizeY = 12;

const playerTextureSizeX = 150;
const playerTextureSizeY = 100;

const playerSizeX = 120;
const playerSizeY = 80;

const wrapTextureToUrl = (s) => `url(${s})`;

const textures = {
    shoot: (frame) => `textures/players/rifle/shoot/survivor-shoot_rifle_${Math.floor(frame / 2)}.png`,
    idle: (frame) => `textures/players/rifle/idle/survivor-idle_rifle_${frame}.png`,
    move: (frame) => `textures/players/rifle/move/survivor-move_rifle_${frame}.png`,
    reload: (frame) => `textures/players/rifle/reload/survivor-reload_rifle_${frame}.png`,
};
