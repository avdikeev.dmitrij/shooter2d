class Player {
    constructor(id, isCamBound = false, posX = null, posY = null) {
        this.id = id;

        const p = document.createElement('div');
        p.id = id;
        const isUser = isCamBound ? 'user' : 'enemy';
        p.style.width = `${playerSizeX}px`;
        p.style.height = `${playerSizeY}px`;
        p.className = `player ${isUser} ${_DEV ? 'bbox' : ''}`;

        const pNick = document.createElement('div');
        pNick.innerText = nickName;
        pNick.className = 'player-nickname';
        p.appendChild(pNick);

        const pTexture = document.createElement('img');
        pTexture.className = `player-texture`;
        pTexture.style.width = `${playerTextureSizeX}px`;
        pTexture.style.left = `${playerSizeX / 2 - playerTextureSizeX / 2}px`;
        pTexture.style.top = `${playerSizeY / 2 - playerTextureSizeY / 2}px`;
        pTexture.style.height = `${playerTextureSizeY}px`;
        p.appendChild(pTexture);

        gameContainer.append(p);

        this.target = $(`#${id}`);

        this.textureTarget = $(`#${id} .player-texture`);

        this.isReloading = false;
        this.isShooting = false;

        this.isCamBound = isCamBound;

        this.angle = 0;

        // top, right, bottom, left (clockwise)
        this.isMoving = [false, false, false, false];

        this.animationFrame = 0;

        this.ammo = 30;

        this.hp = 100;

        this.hitInterval = null;
        this.hitTimeout = null;

        const uf = document.createElement('div');
        uf.className = 'user-fire';
        uf.id = this.id;
        uf.innerHTML = '<div class="fire1"></div><div class="fire2"></div><div class="fire3"></div>';
        gameContainer.append(uf);

        this.userFire = $(`.user-fire#${this.id}`);

        if (posX) {
            this.target.attr('left', posX);
        }
        if (posY) {
            this.target.attr('top', posY);
        }
    }

    onMapChanged() {
        const spawnX = spawn1Coords[0];
        const spawnY = spawn1Coords[1];

        this.setPosition(spawnX, spawnY);
    }

    onConnectedServer() {
        this.allEnemies = players.filter(_ => _.id !== this.id);

        const spawnX = spawn1Coords[0];
        const spawnY = spawn1Coords[1];

        const spawn2X = spawn2Coords[0];
        const spawn2Y = spawn2Coords[1];

        const _x = spawnX + playerSizeX / 2;
        const _y = spawnY + playerSizeY / 2;

        const pC = isPlayerCollision(this.target, this.allEnemies, _x, _y, _x, _y);

        if (pC.left || pC.top) {
            this.setPosition(spawn2X, spawn2Y);
        } else {
            this.setPosition(spawnX, spawnY);
        }
    }

    get isUpMove() { return this.isMoving[0]; };
    get isRightMove() { return this.isMoving[1]; };
    get isDownMove() { return this.isMoving[2]; };
    get isLeftMove() { return this.isMoving[3]; };

    get posX() { return parseInt(this.target.attr('left'), 10); };
    get posY() { return parseInt(this.target.attr('top'), 10); };

    hit(newHP) {
        this.hp = newHP;
        clearInterval(this.hitInterval);
        clearTimeout(this.hitTimeout);
        const _rect = $(".hit-red-rect");

        _rect.removeClass('show');
        {
            _rect.toggleClass('show');
            this.hitInterval = setInterval(() => {
                _rect.toggleClass('show');
            }, 250);

            this.hitTimeout = setTimeout(() => {
                clearInterval(this.hitInterval);
                _rect.removeClass('show');
            }, 2000)
        }
        // console.log(userViewCircleRect);
    }

    setPosition(x, y) {
        mapOffsetX = x - gameWindowW / 2;
        mapOffsetY = y - gameWindowH / 2;

        if (mapOffsetX + gameWindowW > mapSizeX * blockSize - blockSize * 1.5) {
            mapOffsetX = mapSizeX * blockSize - blockSize / 2 - gameWindowW;
            this.target.attr('left', `${x + (blockSize / 2 - playerSizeX / 2)}`);
        } else if (mapOffsetX < blockSize / 2) {
            mapOffsetX = blockSize / 2;
            this.target.attr('left', `${x + (blockSize / 2 - playerSizeX / 2)}`);
        } else {
            this.target.attr('left', `${mapOffsetX + gameWindowW / 2 + (blockSize / 2 - playerSizeX / 2)}`);
        }

        if (mapOffsetY + gameWindowH > mapSizeY * blockSize - blockSize * 1.5) {
            mapOffsetY = mapSizeY * blockSize - blockSize / 2 - gameWindowH;
            this.target.attr('top', `${y + (blockSize / 2 - playerSizeY / 2)}`);
        } else if (mapOffsetY < blockSize / 2) {
            mapOffsetY = blockSize / 2;
            this.target.attr('top', `${y + (blockSize / 2 - playerSizeY / 2)}`);
        } else {
            this.target.attr('top', `${mapOffsetY + gameWindowH / 2 + (blockSize / 2 - playerSizeY / 2)}`);
        }
    }

    setData(x, y, isReloading, isShooting, isMoving, angle) {
        this.target.attr('left', x);
        this.target.attr('top', y);
        this.isReloading = isReloading;
        if (isReloading) this.ammo = 30;
        if (this.isShooting && !isShooting) {
            stopAudio(sfx.ak47);
        }
        this.isShooting = isShooting;
        this.isMoving = isMoving;
        this.angle = angle;
    }

    move(time) {
        let px = 0, py = 0;

        time /= 10;

        this.isMoving[0] ? py = -speed * time : null;
        this.isMoving[1] ? px = speed * time : null;
        this.isMoving[2] ? py = speed * time : null;
        this.isMoving[3] ? px = -speed * time : null;

        const { target } = this;

        let left = parseInt(target.attr('left'), 10);
        let top = parseInt(target.attr('top'), 10);

        let newLeft = left + px;
        let newTop = top + py;

        this.allEnemies = players.filter(_ => _.id !== this.id);

        const playerCollision = isPlayerCollision(this.target, this.allEnemies, left, top, newLeft, newTop);

        const wallCollision = isWallCollision(this.target, left, top, newLeft, newTop);

        if (!wallCollision.left && !playerCollision.left) {
            target.attr('left', newLeft);

            if (newLeft > mapSizeX * blockSize - gameWindowW / 2 - playerSizeX / 2 - blockSize / 2) {
                const left = newLeft - (mapSizeX * blockSize - blockSize / 2 - gameWindowW);
                target.css('left', `${left}px`);
            } else if (newLeft < gameWindowW / 2 + blockSize / 2 - playerSizeX / 2) {
                const left = newLeft - blockSize / 2;
                target.css('left', `${left}px`);
            } else {
                target.css('left', `${gameWindowW / 2 - playerSizeX / 2}px`);
                mapOffsetX = newLeft - gameWindowW / 2 + playerSizeX / 2;
            }
        }

        if (!wallCollision.top && !playerCollision.top) {
            target.attr('top', newTop);

            if (newTop > mapSizeY * blockSize - gameWindowH / 2 - playerSizeY / 2 - blockSize / 2) {
                const top = newTop - (mapSizeY * blockSize - blockSize / 2 - gameWindowH);
                target.css('top', `${top}px`);
            } else if (newTop < gameWindowH / 2 + blockSize / 2 - playerSizeY / 2) {
                const top = newTop - blockSize / 2;
                target.css('top', `${top}px`);
            } else {
                target.css('top', `${gameWindowH / 2 - playerSizeY / 2}px`);
                mapOffsetY = newTop - gameWindowH / 2 + playerSizeY / 2;
            }
        }

        // bindUserView();
        // if (target.hasClass('enemy')) generateDirection();
    }

    draw() {
        if (this.isCamBound) return;

        this.textureTarget.css('transform', `rotate(${this.angle}deg)`);

        const left = parseInt(this.target.attr('left'), 10);
        const top = parseInt(this.target.attr('top'), 10);

        this.target.css('left', `${left - mapOffsetX}px`);
        this.target.css('top', `${top - mapOffsetY}px`);
    }

    animation(tick) {
        const { target, textureTarget } = this;

        let idle = this.isMoving.every(_ => _ === false);

        const removeClassFrom = (t, classNames) => classNames.map(_ => t.removeClass(_));

        const moveCheck = () => {
            const { isLeftMove, isUpMove, isDownMove } = this;

            if (isLeftMove) {
                target.addClass('moveA');
                this.userFire.addClass('moveA');
            } else if (isUpMove) {
                target.addClass('moveW');
                this.userFire.addClass('moveW');
            } else if (isDownMove) {
                target.addClass('moveS');
                this.userFire.addClass('moveS');
            } else {
                target.addClass('moveD');
            }
        };

        if (this.isReloading) {
            textureTarget.attr('src', textures.reload(this.animationFrame));

            if (!idle) {
                removeClassFrom(target, ['moveA', 'moveW', 'moveS', 'moveD']);
                removeClassFrom(this.userFire, ['moveA', 'moveW', 'moveS', 'moveD']);
            }

            moveCheck();

            if (new Date().getTime() - 2000 > this.reloadStartedTime) {
                this.ammo = 30;
                this.isReloading = false;
            }
        } else if (this.isShooting) {
            if (Math.floor(this.animationFrame / 2) > 2) this.animationFrame = 0;
            textureTarget.attr('src', textures.shoot(this.animationFrame));

            if (tick % 7 === 0) this.shoot();
        } else if (idle) {
            textureTarget.attr('src', textures.idle(this.animationFrame));
        } else {
            removeClassFrom(this.userFire, ['moveA', 'moveW', 'moveS', 'moveD'])
            removeClassFrom(target, ['moveA', 'moveW', 'moveS', 'moveD']);

            moveCheck();

            textureTarget.attr('src', textures.move(this.animationFrame));
        }

        if (tick % 5 === 0) this.animationFrame++;
        if (this.animationFrame > 19) this.animationFrame = 0;
    }

    rotate() {
        this.textureTarget.css('transform', `rotate(${this.angle}deg)`);
        if (_DEV) $('.ray').remove();

        $(".block").removeClass('light');

        const rayWalls = [];

        for (let i = 0; i < raysCount; i++) {
            if (i % 4 !== 0) continue;
            for (let j = 0; j < 700; j++) {
                if (rayWalls.includes(i)) continue;
                if (j % 100 !== 0) continue;

                let ray;

                if (_DEV) {
                    ray = document.createElement('div');
                    ray.className = 'ray';
                }

                const p = {
                    x: parseInt(this.target.css('left'), 10),
                    y: parseInt(this.target.css('top'), 10),
                };

                const _ray = {
                    x: p.x + playerSizeX / 2 + j,
                    y: p.y + playerSizeY / 2
                }
                const rayAngle = (this.angle + 180) - raysCount / 2 + i;

                const rayAngleRadians = rayAngle * Math.PI / 180;

                const center = { x: p.x + playerSizeX / 2, y: p.y + playerSizeY / 2 };

                let rotatedX = center.x + (center.x - _ray.x) * Math.cos(rayAngleRadians) - (center.y - _ray.y) * Math.sin(rayAngleRadians);
                let rotatedY = center.y + (center.x - _ray.x) * Math.sin(rayAngleRadians) + (center.y - _ray.y) * Math.cos(rayAngleRadians);

                if (_DEV) {
                    ray.style.height = '1px';
                    ray.style.width = '1px';
                    ray.style.left = `${rotatedX}px`;
                    ray.style.top = `${rotatedY}px`;
                }

                const blockX = Math.floor((mapOffsetX + rotatedX) / blockSize);
                const blockY = Math.floor((mapOffsetY + rotatedY) / blockSize);

                const _block = $(`.block[left='${blockX}'][top='${blockY}']`);
                if (_block.hasClass('black')) {
                    rayWalls.push(i);
                }
                _block.addClass('light');

                players.filter(_ => _.id !== this.id).forEach(_ => {
                    _.target.removeClass('visible');
                    const pX = Math.floor(_.posX / blockSize);
                    const pY = Math.floor(_.posY / blockSize);
                    const _block = $(`.block[left='${pX}'][top='${pY}']`);
                    if (_block.hasClass('light')) {
                        _.target.addClass('visible');
                    }
                });

                if (_DEV) gameContainer[0].appendChild(ray);
            }
        }
    }

    update(tick, time) {
        if (this.isCamBound) {
            this.rotate();
            this.move(time);
        }

        this.animation(tick);
    }

    reload() {
        this.isReloading = true;
        this.reloadStartedTime = new Date().getTime();
    }

    shoot() {
        if (this.ammo <= 0) {
            stopAudio(sfx.ak47);
            return;
        }

        const { userFire } = this;

        if (this.target.hasClass('visible') || this.isCamBound) {
            playAudio(sfx.ak47, 1, 0.1);
        }

        userFire.css('opacity', 1);
        setTimeout(() => userFire.css('opacity', 0), 100);

        this.ammo -= 1;

        const x = parseInt(this.target.css('left'), 10);
        const y = parseInt(this.target.css('top'), 10);

        const a = this.angle * Math.PI / 180;

        const center = { x: x + playerSizeX / 2, y: y + playerSizeY / 2 };

        const _x = center.x + (center.x - x + 20) * Math.cos(a) - (center.y - y - 20) * Math.sin(a);
        const _y = center.y + (center.x - x + 20) * Math.sin(a) + (center.y - y - 20) * Math.cos(a);

        userFire.css('left', `${_x}px`);
        userFire.css('top', `${_y}px`);
        userFire.css('transform', `rotate(${this.angle}deg)`);

        userFire.removeClass('visible');
        if (this.target.hasClass('visible') || this.isCamBound) {
            userFire.addClass('visible');
        }

        if (this.isCamBound) {
            if (connectedToServer) {
                socket.emit('shot', { angle: this.angle, position: { x: _x + mapOffsetX, y: _y + mapOffsetY } });
            } else {
                bullets.push({ angle: this.angle, position: { x: _x + mapOffsetX, y: _y + mapOffsetY } });
            }
        }
    }
}


