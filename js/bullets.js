const drawBullets = (time) => {
    time /= 10;

    $('.bullet').remove();
    const remove = [];

    if (connectedToServer) {
        bullets.forEach((_, k) => {
            const bullet = document.createElement('div');
            const vertical = [0, 2].includes(_.direction) ? ' vertical' : '';
            bullet.className = `${_DEV ? 'bbox' : ''} bullet` + vertical;
            bullet.style.left = `${_.position.x - mapOffsetX}px`;
            bullet.style.top = `${_.position.y - mapOffsetY}px`;
            $(bullet).attr('left', _.position.x - mapOffsetX);
            $(bullet).attr('top', _.position.y - mapOffsetY);
            $(".border").append(bullet);

            const pc = isPlayerCollision($(bullet), [player], _.position.x, _.position.y, _.position.x, _.position.y);
            if (pc.left || pc.top) {

            }
        });

    } else {
        bullets.forEach((_, k) => {
            const bullet = document.createElement('div');
            const vertical = [0, 2].includes(_.direction) ? ' vertical' : '';
            bullet.className = `${_DEV ? 'bbox' : ''} bullet` + vertical;
            bullet.style.left = `${_.position.x - mapOffsetX}px`;
            bullet.style.top = `${_.position.y - mapOffsetY}px`;
            $(".border").append(bullet);

            let _bulletSpeed = bulletSpeed * time;

            const a = _.angle * Math.PI / 180;

            const newX = _.position.x + _bulletSpeed * Math.cos(a);
            const newY = _.position.y + _bulletSpeed * Math.sin(a);

            if (blocks[Math.floor(newY / blockSize)][Math.floor(newX / blockSize)] > 0) {
                remove.push(k);
            } else {
                bullets[k].position.x = newX;
                bullets[k].position.y = newY;
            }
        });

        bullets = bullets.filter((_, k) => !remove.includes(k));
    }
};
