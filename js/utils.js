const rand = (min, max) => Math.round(min + Math.random() * (max - min));

const fract = (x) => x - Math.floor(x);

const getJson = async (url, params) => {
    let paramStr = '';
    if (params) {
        paramStr = '?';
        Object.keys(params).forEach(_ => {
            paramStr += _ + '=' + params[_] + '&';
        });
    }
    const r = await fetch(server + url + paramStr);
    const d = await r.json();
    return d;
}

const bindUserView = () => {
    const userView = $(".user-view-circle");

    const userLeft = parseInt(player.target.css('left'), 10);
    const userTop = parseInt(player.target.css('top'), 10);

    userView.attr('cx', userLeft + playerSizeX / 2);
    userView.attr('cy', userTop + playerSizeY / 2);
};

const lockCursor = () => {
    const element = document.getElementsByClassName('border')[0];

    element.requestPointerLock = element.requestPointerLock ||
        element.mozRequestPointerLock ||
        element.webkitRequestPointerLock;
// Ask the browser to lock the pointer
    element.requestPointerLock();

// Ask the browser to release the pointer
    document.exitPointerLock = document.exitPointerLock ||
        document.mozExitPointerLock ||
        document.webkitExitPointerLock;
    document.exitPointerLock();
};
