const isWallCollision = (player, oldX, oldY, newX, newY) => {
    let corner1, corner2, corner3, corner4;

    corner1 = [Math.floor((newX) / blockSize), Math.floor((oldY) / blockSize)];
    corner2 = [Math.floor((newX + playerSizeX) / blockSize), Math.floor((oldY) / blockSize)];
    corner3 = [Math.floor((newX) / blockSize), Math.floor((oldY + playerSizeY) / blockSize)];
    corner4 = [Math.floor((newX + playerSizeX) / blockSize), Math.floor((oldY + playerSizeY) / blockSize)];

    const collision1X = blocks[corner1[1]][corner1[0]] > 0;
    const collision2X = blocks[corner2[1]][corner2[0]] > 0;
    const collision3X = blocks[corner3[1]][corner3[0]] > 0;
    const collision4X = blocks[corner4[1]][corner4[0]] > 0;

    corner1 = [Math.floor((oldX) / blockSize), Math.floor((newY) / blockSize)];
    corner2 = [Math.floor((oldX + playerSizeX) / blockSize), Math.floor((newY) / blockSize)];
    corner3 = [Math.floor((oldX) / blockSize), Math.floor((newY + playerSizeY) / blockSize)];
    corner4 = [Math.floor((oldX + playerSizeX) / blockSize), Math.floor((newY + playerSizeY) / blockSize)];

    const collision1Y = blocks[corner1[1]][corner1[0]] > 0;
    const collision2Y = blocks[corner2[1]][corner2[0]] > 0;
    const collision3Y = blocks[corner3[1]][corner3[0]] > 0;
    const collision4Y = blocks[corner4[1]][corner4[0]] > 0;

    return {
        left: collision1X || collision2X || collision3X || collision4X,
        top: collision1Y || collision2Y || collision3Y || collision4Y,
    };
};

const isPlayerCollision = (player, players, oldX, oldY, x, y) => {
    let left = false;
    let top = false;

    for (let i = 0; i < players.length; i++) {
        let enemyX = parseInt(players[i].target.attr('left'), 10);
        let enemyY = parseInt(players[i].target.attr('top'), 10);

        if (Math.abs(enemyX - x) < playerSizeX) {
            if (Math.abs(enemyY - oldY) < playerSizeY) {
                left = true;
            }
        }

        if (Math.abs(enemyX - oldX) < playerSizeX) {
            if (Math.abs(enemyY - y) < playerSizeY) {
                top = true;
            }
        }
    }

    return { left, top };
}
