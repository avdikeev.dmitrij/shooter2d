let clientX = gameWindowW / 2;
let clientY = gameWindowH / 2;

const sensitivity = 0.2;

gameContainer[0].addEventListener('mousemove', (e) => {
    const { movementX, movementY} = e;

    clientX += movementX * sensitivity;

    if (clientX < gameWindowW / 2.5) {
        clientX = gameWindowW / 2.5;
    }

    if (clientX > gameWindowW / 1.5) {
        clientX = gameWindowW / 1.5;
    }

    clientY += movementY * sensitivity;

    if (clientY > gameWindowH / 1.5){
        clientY = gameWindowH / 1.5;
    }
    if (clientY < gameWindowH / 2.5) {
        clientY = gameWindowH / 2.5;
    }

    const centerX = gameWindowW / 2;
    const centerY = gameWindowH / 2;

    player.angle = Math.atan2(clientX - centerX, -(clientY - centerY)) * (180 / Math.PI) - 90;
});

const mouseHandler = (key, state) => {
    switch (key) {
        case 0:
            if (!state) stopAudio(sfx.ak47);
            player.isShooting = state;
            break;
    }
};

gameContainer[0].addEventListener('mousedown', (e) => {
    mouseHandler(e.button, true);
});

gameContainer[0].addEventListener('mouseup', (e) => {
    mouseHandler(e.button, false);
});
