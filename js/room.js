const socket = io.connect('http://localhost:3000');

const roomButtonLabel = (_) => `${_.name} (${_.players.length}/${_.maxPlayers})`;

const roomListListener = () => {
    socket.on('room-list', ({ rooms }) => {
        socket.removeAllListeners('room-list');
        $(".rooms-list").html("");
        rooms.forEach(_ => {
            $(".rooms-list").append(`<button id="${_.id}" class="myButton">${roomButtonLabel(_)}</button>`);
        });

        $(".rooms-list button").click('click', async ({ target }) => {
            lockCursor();
            const roomId = $(target).attr('id');
            socket.on('accept-player', ({ success, room, msg, playerId }) => {
                $(target).text(roomButtonLabel(room)).attr('disabled', 'disabled');

                {
                    menu.slideUp();
                    gameStarted = true;
                }

                connectedToServer = false;
                if (success) {
                    connectedToServer = true;
                    const addPlayer = (player) => {
                        if (player.id === playerId) return;
                        players.push(new Player(player.id, false, player.x, player.y));
                    };

                    room.players.forEach(player => {
                        addPlayer(player);
                    });

                    currentMap = maps[room.map];
                    drawMap(currentMap);

                    player.onConnectedServer();

                    socket.on('game-info', ({ room }) => {
                        room.players.forEach(player => {
                            const P = players.find(_ => _.id === player.id);
                            if (P) {
                                P.setData(player.x, player.y, player.isReloading, player.isShooting, player.isMoving, player.angle);
                            }
                        });

                        const me = room.players.find(p => p.id === playerId);
                        if (me.hp < player.hp) {
                            player.hit(me.hp);
                        }

                        bullets = room.bullets;
                        socket.emit('pos', {
                            x: player.posX,
                            y: player.posY,
                            isReloading: player.isReloading,
                            isShooting: player.isShooting,
                            isMoving: player.isMoving,
                            angle: player.angle,
                        })
                    });

                    socket.on('player-connected', ({ player }) => {
                        addPlayer(player);
                    });

                    socket.on('player-disconnected', ({ id }) => {
                        const pIndex = players.findIndex(_ => _.id === id);
                        const p = players[pIndex];
                        $(`#${p.id}`).remove();
                        delete players[pIndex];
                        players = players.filter(Boolean);
                    });
                }
                socket.removeAllListeners('accept-player');
            });
            socket.emit('join-room', { id: roomId });
        })
    })
};

setTimeout(() => {
    if (!socket.connected) socket.disconnect();
}, 1000);

roomListListener();
