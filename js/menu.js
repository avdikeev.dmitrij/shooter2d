const menu = $(".menu");
const startScreen = $(".startScreen");
const menuSelectMap = $(".menuSelectMap");
const roomsListContainer = $(".rooms");
const sandboxButton = $(".menu .button1");
const onlineButton = $(".menu .button2");
const settingsButton = $(".menu .button3");
const mainMenu = $(".menu .button0");
const availableMap = $(".availableMap");
const settingsContainer = $(".menuSettings");

mainMenu.click(() => {
    startScreen.css('display', 'flex');
    menuSelectMap.css('display', 'none');
    settingsContainer.css('display', 'none');
    roomsListContainer.css('display', 'none');
});

sandboxButton.click(() => {
    startScreen.hide();
    menuSelectMap.css('display', 'flex');
});

onlineButton.click(() => {
    startScreen.hide();
    roomsListContainer.css('display', 'flex');
});

availableMap.click(({ target }) => {
    menu.slideUp();
    currentMap = maps[target.innerText];
    drawMap(currentMap);
    gameStarted = true;
    lockCursor();
});

settingsButton.click(() => {
    startScreen.hide();
    settingsContainer.css('display', 'flex');
});

$("#nicknameInput").val(localStorage.getItem('nickname'));

$("#nicknameInput").on('input', ({ target }) => {
    nickName = target.value;
    localStorage.setItem('nickname', nickName);
});

// setTimeout(() => availableMap[0].click(), 100);
