let nowSfxPlaying = null;
let sfxLoopInterval = null;

const sfx = {
    ak47: document.getElementById('ak47sfx'),
};

const playAudio = (sound, time, maxTime) => {
    if (nowSfxPlaying && $(nowSfxPlaying).id === $(sound).id) {
        return;
    }

    nowSfxPlaying = sound;
    if (time) {
        sound.currentTime = time;
    }

    if (maxTime) {
        sfxLoopInterval = setInterval(() => {
            sound.currentTime = time;
        }, maxTime * 1000);
    }

    sound.play();
};

const stopAudio = (sound) => {
    nowSfxPlaying = null;
    clearInterval(sfxLoopInterval);
    sound.currentTime = 0;
    sound.pause();
};
