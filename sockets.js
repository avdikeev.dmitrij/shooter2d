const io = require('socket.io')();

const rooms = [
    {
        id: 1,
        name: 'Тестовая комната',
        maxPlayers: 2,
        players: [],
        map: 'de_dust',
        bullets: [],
    }
];

let clients = [];

const bulletSpeed = 2;

const playerSizeX = 120;
const playerSizeY = 80

let clock = new Date().getTime();

setInterval(() => {
    let elapsed = new Date().getTime() - clock;
    clock = new Date().getTime();

    rooms.forEach((room, roomIndex) => {
        const { players, bullets } = room;

        const removedBullets = [];

        bullets.forEach((_, k) => {
            let _bulletSpeed = bulletSpeed * elapsed;

            const a = _.angle * Math.PI / 180;

            const newX = _.position.x + _bulletSpeed * Math.cos(a);
            const newY = _.position.y + _bulletSpeed * Math.sin(a);

            const isBulletCollision = (player, x, y) => {
                let pX = parseInt(player.x, 10);
                let pY = parseInt(player.y, 10);

                let pX2 = pX + playerSizeX;
                let pY2 = pY + playerSizeY;

                if (x > pX && x < pX2) {
                    if (y > pY && y < pY2) {
                        return true;
                    }
                }

                return false;
            }

            players.forEach((pl, pKey) => {
                if (pl.id !== _.owner && isBulletCollision(pl, _.position.x, _.position.y)) {
                    rooms[roomIndex].players[pKey].hp -= 2.5;
                    removedBullets.push(k);
                }
            });

            // if (blocks[Math.floor(newY / blockSize)][Math.floor(newX / blockSize)] > 0) {
            if (_.position.x < 0 || _.position.x > 4000 || _.position.y < 0 || _.position.y > 3000) {
                removedBullets.push(k);
            } else {
                rooms[roomIndex].bullets[k].position.x = newX;
                rooms[roomIndex].bullets[k].position.y = newY;
            }
        });

        rooms[roomIndex].bullets = bullets.filter((_, k) => !removedBullets.includes(k));


        players.forEach(player => {
            const client = clients.find(cl => cl.id === player.id);
            client.emit('game-info', { room });
        })
    });
}, 10);

io.on('connection', client => {
    clients.push(client);

    client.on('disconnect', () => {
        rooms.forEach((_, k) => {
            if (_.players.map(__ => __.id).includes(client.id)) {
                rooms[k].players = _.players.filter(_ => _.id !== client.id);
                rooms[k].players.forEach(pl => {
                    const cl = clients.find(cl => cl.id === pl.id);
                    cl.emit('player-disconnected', { id: client.id });
                });
            }
        });

        clients = clients.filter(_ => _.id !== client.id);
        client.removeAllListeners();
    })

    client.emit('room-list', {rooms});

    client.on('join-room', ({id}) => {
        let msg;
        const roomIndex = rooms.findIndex(_ => _.id.toString() === id.toString());
        if (roomIndex !== -1) {
            const room = rooms[roomIndex];
            if (!room.players.map(p => p.id).includes(client.id) && room.maxPlayers > room.players.length) {
                const newPlayer = {
                    id: client.id,
                    x: 0,
                    y: 0,
                    isReloading: false,
                    isShooting: false,
                    isMoving: [false, false, false, false],
                    hp: 100,
                };
                rooms[roomIndex].players.push(newPlayer);

                // const playerIndex = room.players.findIndex(p => p.id === client.id);

                client.emit('accept-player', {success: true, room, playerId: client.id});
                client.on('pos', ({ x, y, isReloading, isShooting, isMoving, angle }) => {
                    const playerIndex = room.players.findIndex(p => p.id === client.id);
                    rooms[roomIndex].players[playerIndex].x = x;
                    rooms[roomIndex].players[playerIndex].y = y;
                    rooms[roomIndex].players[playerIndex].isReloading = isReloading;
                    rooms[roomIndex].players[playerIndex].isShooting = isShooting;
                    rooms[roomIndex].players[playerIndex].isMoving = isMoving;
                    rooms[roomIndex].players[playerIndex].angle = angle;
                });

                client.on('shot', ({ angle, position }) => {
                    rooms[roomIndex].bullets.push({ owner: client.id, angle, position });
                });

                room.players.forEach(_ => {
                    const cl = clients.find(__ => __.id === _.id && __.id !== client.id);
                    if (!cl) return;
                    cl.emit('player-connected', { player: newPlayer });
                });
            } else {
                msg = room.maxPlayers > room.players.length ? 'already connected' : 'max players';
            }
        } else {
            msg = 'room not found';
        }
        client.emit('accept-player', {success: false, room: null, msg});
    });
});

io.listen(3000);
