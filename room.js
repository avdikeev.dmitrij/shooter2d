var ip = require("ip");
const http = require('http')
const url = require('url');
const qs = require('querystring');

// var io = require('socket.io')(80);

let _ip = ip.address();

const kickSecondsDelay = 5;

const rooms = [
    {
        id: 1,
        name: 'Тестовая комната',
        maxPlayers: 2,
        players: [],
        map: 'de_dust',
        bullets: [],
    }
];

const getRoomForPlayer = (roomId, playerId) => {
    const roomI = rooms.findIndex(_ => _.id.toString() === roomId.toString());
    if (roomI !== -1) {
        const room = rooms[roomI];
        const playerI = room.players.findIndex(_ => _.id.toString() === playerId.toString());
        if (playerI !== -1) {
            rooms[roomI].players[playerI].lastActivity = new Date().getTime();
            return room;
        }
    }
    return false;
};

setInterval(() => {
    rooms.forEach((room, roomKey) => {
        room.players.forEach((player) => {
            if (player.lastActivity < new Date().getTime() - kickSecondsDelay * 1000) {
                console.log(`Player ${player.id} no activity! Kicked from server ${room.id}`);
                rooms[roomKey].players = room.players.filter(p => p.id !== player.id);
            }
        });
    });
}, 1000);

const jsonRequestHandler = (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
    const _url = url.parse(req.url);

    if (_url.pathname === '/rooms') {
        res.end(JSON.stringify(rooms));
    }
    const _qs = qs.parse(_url.query);

    if (_url.pathname === '/info') {
        const roomId = _qs.id;
        const playerId = _qs.netId;
        const room = getRoomForPlayer(roomId, playerId);
        res.end(JSON.stringify({ room }));
    }

    if (_url.pathname === '/room') {
        const id = _qs.id;
        const playerId = _qs.netId;
        const room = rooms.find(_ => _.id.toString() === id);
        let success = false;
        if (room.maxPlayers > room.players) {
            success = true;
            room.players.push({
                id: playerId,
                x: 0,
                y: 0,
                lastActivity: new Date().getTime(),
            });
        }
        res.end(JSON.stringify({ success, room }));
    }
};

const server2 = http.createServer(jsonRequestHandler);

server2.listen('3003', _ip, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${_ip}:3003`);
})
