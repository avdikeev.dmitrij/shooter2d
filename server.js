// содежимое index.js
const http = require('http')
const fs = require('fs');
const fsp = fs.promises;
const util = require('util');
const port = 3000



const loadHTML = (filePath, response) => {
    fs.readFile(__dirname + "/" + filePath)
        .then(contents => {
            response.setHeader("Content-Type", "text/html");
            response.writeHead(200);
            response.end(contents);
        })
};

const loadCss = (filePath, response) => {
    fs.readFile(__dirname + "/" + filePath)
        .then(contents => {
            response.setHeader("Content-Type", "text/css");
            response.writeHead(200);
            response.end(contents);
        })
};

const requestHandler = (request, response) => {
    let filePath = request.url;

    if (request.url === '/favicon.ico') {
        response.end();
        return;
    }

    if (request.url === '/') {
        filePath = '/index.html';
    }
    fsp.readFile(__dirname + filePath)
        .then((data, err) => {
        if (!err) {
            var dotoffset = filePath.lastIndexOf('.');
            var mimetype = dotoffset == -1
                ? 'text/plain'
                : {
                    '.html' : 'text/html',
                    '.ico' : 'image/x-icon',
                    '.jpg' : 'image/jpeg',
                    '.png' : 'image/png',
                    '.gif' : 'image/gif',
                    '.css' : 'text/css',
                    '.js' : 'text/javascript',
                    '.mp3' : 'audio/mpeg',
                }[ filePath.substr(dotoffset) ];


            if (mimetype === 'audio/mpeg') {
                var range = request.headers.range.replace("bytes=", "").split('-');

                var stats = fs.statSync(__dirname + filePath);
                var fileSizeInBytes = stats["size"];

                var bytes_start = 0;
                var bytes_end = range[1] ? parseInt(range[1], 10) : fileSizeInBytes;

                var chunk_size = bytes_end - bytes_start;

                if (chunk_size == fileSizeInBytes) {
                    // Serve the whole file as before
                    response.writeHead(200, {
                        "Accept-Ranges": "bytes",
                        'Content-Type': 'audio/mpeg',
                        'Content-Length': fileSizeInBytes});
                    // filestream.pipe(response);
                    response.end(data);
                } else {
                    // HTTP/1.1 206 is the partial content response code
                    console.log(bytes_start, bytes_end);
                    response.writeHead(206, {
                        "Content-Range": "bytes " + bytes_start + "-" + bytes_end + "/" + fileSizeInBytes,
                        "Accept-Ranges": "bytes",
                        'Content-Type': 'audio/mpeg',
                        'Content-Length': fileSizeInBytes
                    });
                    response.end(data.slice(bytes_start, bytes_end));
                }
            } else {
                response.setHeader('Content-type' , mimetype);
                response.end(data);
            }
            // console.log( request.url, mimetype );
        } else {
            console.log ('file not found: ' + request.url);
            response.writeHead(404, "Not Found");
            response.end();
        }
    });
}

const server = http.createServer(requestHandler)
server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`local game server is listening on ${port}`);
})


