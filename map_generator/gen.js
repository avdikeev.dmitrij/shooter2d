const bs = 40;

const x = 640 / bs;
const y = 480 / bs;

let spawn = null;

$(".spawn1").click(() => spawn = 1);
$(".spawn2").click(() => spawn = 2);
$(".block-select").click(() => spawn = null);

for (let i = 0; i < x; i++) {
  for (let j = 0; j < y; j++) {
    const b = document.createElement('div');
    b.className = 'block';
    b.style.left = `${bs * i}px`;
    b.style.top = `${bs * j}px`;
    $(b).attr('x', i);
    $(b).attr('y', j);
    $('.border').append(b);
  }
}

let mousedown = false;

let toBlack = false;

$(window).mousedown(({ target }) => {
  if (spawn === 1) {
    if ($(target).parent()[0].className !== 'border') return;
    $('.block').removeClass('spawn1');
    $(target).toggleClass('spawn1');
    spawn = null;
    return;
  }

  if (spawn === 2) {
    if ($(target).parent()[0].className !== 'border') return;
    $('.block').removeClass('spawn2');
    $(target).toggleClass('spawn2');
    spawn = null;
    return;
  }

  mousedown = true;
  const isBlack = $(target).hasClass('black');

  if (isBlack) {
    toBlack = false;
  } else {
    toBlack = true;
  }
});
$(window).mouseup(() => mousedown = false);

$(window).mousemove(({ target }) => {
  if ($(target).parent()[0].className !== 'border') return;

  if (!mousedown) return;

  const isBlack = $(target).hasClass('black');

  if (isBlack && toBlack) return;
  if (!isBlack && !toBlack) return;
  // if (!$(target).hasClass('black') && !toBlack) return;

  $(target).toggleClass('black');

  $(target).removeClass('spawn1');
  $(target).removeClass('spawn2');
});

const map = [];
let line = [];
let lastY = 0;

for(let i = 0; i < y; i++) {
  for (let j = 0; j < x; j++) {
    line.push(0);
  }
  map.push(line);
  line = [];
}

$('button').click(() => {
  $('.border .block').each(function() {
    const x = $(this).attr('x');
    const y = $(this).attr('y');
    const isSpawn1 = $(this).hasClass('spawn1');
    const isSpawn2 = $(this).hasClass('spawn2');
    const isBlack = $(this).hasClass('black');
    map[y][x] = (isSpawn1 ? -1 : (isSpawn2 ? -2 : (isBlack ? 1 : 0)));
  })
  $('textarea').val(JSON.stringify(map));
})